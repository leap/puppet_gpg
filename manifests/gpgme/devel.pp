class gpg::gpgme::devel {
  include gpg::gpgme

  case $::operatingsystem {
    debian: { $gpgme_devel = 'libgpgme11-dev' }
    default: { $gpgme_devel = 'libgpgme11-dev' }
  }

  package{$gpgme_devel:
    ensure  => installed,
    require => Package['gpgme'],
  }
}
