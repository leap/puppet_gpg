class gpg::gpgme {
  case $::operatingsystem {
    debian: { include gpg::gpgme::debian }
    default: { include gpg::gpgme::base }
  }
}
